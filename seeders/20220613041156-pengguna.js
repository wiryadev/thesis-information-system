"use strict";
const fs = require("fs");
const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync("./seeders/data/pengguna.json"));
    const pengguna = data.map((element) => {
      return {
        biodataId: element.biodataId,
        uuid: uuidv4(),
        email: element.email,
        password: bcrypt.hashSync(element.password, 10),
        active: element.active,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("Penggunas", pengguna);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Penggunas", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
