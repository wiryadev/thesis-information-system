"use strict";
const fs = require("fs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(
      fs.readFileSync("./seeders/data/aksesPengguna.json")
    );
    const aksesPengguna = data.map((element) => {
      return {
        penggunaId: element.penggunaId,
        statusId: element.statusId,
        aksesId: element.aksesId,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("AksesPenggunas", aksesPengguna);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("AksesPenggunas", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
