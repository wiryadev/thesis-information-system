"use strict";
const fs = require("fs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(
      fs.readFileSync("./seeders/data/biodataPengguna.json")
    );
    const biodataPengguna = data.map((element) => {
      return {
        nama: element.nama,
        nip: element.nip,
        nidn: element.nidn,
        nim: element.nim,
        angkatan: element.angkatan,
        no_hp: element.no_hp,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("BiodataPenggunas", biodataPengguna);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("BiodataPenggunas", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
