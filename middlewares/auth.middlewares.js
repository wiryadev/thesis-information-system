const { verifyJwt } = require("../services/jwt.service");
const {
  getUserByUuid,
  getActiveUserAccessByUuid,
} = require("../services/pengguna.service");

class AuthMiddleware {
  static async authentication(req, res, next) {
    try {
      if (!req.headers.authorization) {
        throw {
          code: 406,
          status: "Not Acceptable",
          message: "Anda tidak memiliki access_token",
        };
      }

      const payload = await verifyJwt(req.headers.authorization);
      if (!payload) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Token tidak sah",
        };
      }
      payload.akses = [];

      const user = await getActiveUserAccessByUuid(payload.uuid);

      if (!user) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Token akses anda tidak valid",
        };
      }
      if (!user.access_token) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Silahkan login terlebih dahulu",
        };
      }

      user.AksesPengguna.forEach((element) => {
        payload.akses.push(element.aksesId);
      });

      req.user = payload;
      console.log(req.user);
      next();
    } catch (error) {
      next(error);
    }
  }

  static async authorization(req, res, next, allowedRoles) {
    try {
      let check;
      for (let i = 0; i < allowedRoles.length; i++) {
        for (let j = 0; j < req.user.akses.length; j++) {
          if (req.user.akses[j] == allowedRoles[i]) {
            check = true;
            break;
          }
        }
      }

      if (!check) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda tidak memiliki akses ke layanan ini",
        };
      }
      next();
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthMiddleware;
