const express = require("express");
const app = express();
const errorHanling = require("./helpers/errorHandling.helper");
const routes = require("./routes");
const morgan = require("morgan");
const cors = require("cors");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.use(express.static("public"));

app.use(routes);

app.use(errorHanling);

module.exports = app;
