"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Bimbingan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bimbingan.belongsTo(models.SuratTugasDospem, {
        foreignKey: "suratTugasDospemId",
        as: "SuratTugasDospem",
      });
    }
  }
  Bimbingan.init(
    {
      uuid: DataTypes.UUID,
      suratTugasDospemId: DataTypes.INTEGER,
      topik: DataTypes.STRING,
      catatan: DataTypes.TEXT,
      waktu: DataTypes.DATE,
      tempat: DataTypes.STRING,
      dokumentasi: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Bimbingan",
    }
  );
  return Bimbingan;
};
