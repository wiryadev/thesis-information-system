"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class IdePenelitian extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      IdePenelitian.belongsTo(models.Pengguna, {
        foreignKey: "dosenId",
        as: "Dosen",
      });
    }
  }
  IdePenelitian.init(
    {
      uuid: DataTypes.UUID,
      dosenId: DataTypes.INTEGER,
      judul: DataTypes.STRING,
      deskripsi: DataTypes.TEXT,
      pelaksana: DataTypes.BOOLEAN,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "IdePenelitian",
    }
  );
  return IdePenelitian;
};
