"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PengajuanSeminarSidang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PengajuanSeminarSidang.belongsTo(models.SuratTugasDospem, {
        foreignKey: "suratTugasDospemId",
        as: "SuratTugasDospem",
      });

      PengajuanSeminarSidang.belongsTo(models.SeminarSidang, {
        foreignKey: "seminarSidangId",
        as: "SeminarSidang",
      });

      PengajuanSeminarSidang.hasMany(models.Dokumen, {
        foreignKey: "pengajuanSeminarSidangId",
        as: "DokumenPengajuanSeminarSidang",
      });
    }
  }
  PengajuanSeminarSidang.init(
    {
      uuid: DataTypes.UUID,
      suratTugasDospemId: DataTypes.INTEGER,
      seminarSidangId: DataTypes.INTEGER,
      seminar: DataTypes.BOOLEAN,
      sidang: DataTypes.BOOLEAN,
      persetujuan: DataTypes.BOOLEAN,
      keterangan: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "PengajuanSeminarSidang",
    }
  );
  return PengajuanSeminarSidang;
};
