"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SeminarSidang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SeminarSidang.hasOne(models.PengajuanSeminarSidang, {
        foreignKey: "seminarSidangId",
        as: "PengajuanSeminarSidang",
      });

      SeminarSidang.belongsTo(models.Pengguna, {
        foreignKey: "k_penguji",
        as: "KetuaPenguji",
      });

      SeminarSidang.belongsTo(models.Pengguna, {
        foreignKey: "penguji1",
        as: "Penguji1",
      });

      SeminarSidang.belongsTo(models.Pengguna, {
        foreignKey: "penguji2",
        as: "Penguji2",
      });

      SeminarSidang.hasMany(models.Dokumen, {
        foreignKey: "seminarSidangId",
        as: "DokumenSeminarSidang",
      });
    }
  }
  SeminarSidang.init(
    {
      uuid: DataTypes.UUID,
      waktu: DataTypes.DATE,
      tempat: DataTypes.STRING,
      k_penguji: DataTypes.INTEGER,
      penguji1: DataTypes.INTEGER,
      penguji2: DataTypes.INTEGER,
      keterlaksanaan: DataTypes.BOOLEAN,
      deletedAt: DataTypes.DATE,
      nilaiDospem1: DataTypes.FLOAT,
      nilaiDospem2: DataTypes.FLOAT,
      nilaiKPenguji: DataTypes.FLOAT,
      nilaiPenguji1: DataTypes.FLOAT,
      nilaiPenguji2: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "SeminarSidang",
    }
  );
  return SeminarSidang;
};
