"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Dokumen extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Dokumen.belongsTo(models.SuratTugasDospem, {
        foreignKey: "suratTugasDospemId",
        as: "SuratTugasDospem",
      });

      Dokumen.belongsTo(models.JenisDokumen, {
        foreignKey: "jenisDokumenId",
        as: "JenisDokumen",
      });

      Dokumen.belongsTo(models.PengajuanSeminarSidang, {
        foreignKey: "pengajuanSeminarSidangId",
        as: "PengajuanSeminarSidang",
      });

      Dokumen.belongsTo(models.SeminarSidang, {
        foreignKey: "seminarSidangId",
        as: "SeminarSidang",
      });
    }
  }
  Dokumen.init(
    {
      uuid: DataTypes.UUID,
      jenisDokumenId: DataTypes.INTEGER,
      pengajuanPenelitianDosenId: DataTypes.INTEGER,
      suratTugasDospemId: DataTypes.INTEGER,
      bimbinganId: DataTypes.INTEGER,
      pengajuanSeminarSidangId: DataTypes.INTEGER,
      seminarSidangId: DataTypes.INTEGER,
      filename: DataTypes.STRING,
      filesize: DataTypes.INTEGER,
      mimetype: DataTypes.STRING,
      path: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Dokumen",
    }
  );
  return Dokumen;
};
