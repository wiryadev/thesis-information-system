"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SuratTugasDospem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SuratTugasDospem.belongsTo(models.Pengguna, {
        foreignKey: "mahasiswaId",
        as: "Mahasiswa",
      });

      SuratTugasDospem.belongsTo(models.Pengguna, {
        foreignKey: "pembimbing1",
        as: "Pembimbing1",
      });

      SuratTugasDospem.belongsTo(models.Pengguna, {
        foreignKey: "pembimbing2",
        as: "Pembimbing2",
      });

      SuratTugasDospem.hasMany(models.Dokumen, {
        foreignKey: "suratTugasDospemId",
        as: "Dokumen",
      });

      SuratTugasDospem.hasMany(models.Bimbingan, {
        foreignKey: "suratTugasDospemId",
        as: "Bimbingan",
      });

      SuratTugasDospem.hasMany(models.PengajuanSeminarSidang, {
        foreignKey: "suratTugasDospemId",
        as: "PengajuanSeminarSidang",
      });
    }
  }
  SuratTugasDospem.init(
    {
      uuid: DataTypes.UUID,
      mahasiswaId: DataTypes.INTEGER,
      pembimbing1: DataTypes.INTEGER,
      pembimbing2: DataTypes.INTEGER,
      judul: DataTypes.STRING,
      deskripsi: DataTypes.TEXT,
      keterangan: DataTypes.STRING,
      persetujuan: DataTypes.BOOLEAN,
      expiredAt: DataTypes.DATE,
      nilaiSeminar: DataTypes.FLOAT,
      nilaiSidang: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "SuratTugasDospem",
    }
  );
  return SuratTugasDospem;
};
