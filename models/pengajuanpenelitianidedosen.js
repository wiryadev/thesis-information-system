"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PengajuanPenelitianIdeDosen extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PengajuanPenelitianIdeDosen.belongsTo(models.Pengguna, {
        foreignKey: "mahasiswaId",
        as: "Mahasiswa",
      });
    }
  }
  PengajuanPenelitianIdeDosen.init(
    {
      uuid: DataTypes.UUID,
      idePenelitianId: DataTypes.INTEGER,
      mahasiswaId: DataTypes.INTEGER,
      penerimaan: DataTypes.BOOLEAN,
      keterangan: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "PengajuanPenelitianIdeDosen",
    }
  );
  return PengajuanPenelitianIdeDosen;
};
