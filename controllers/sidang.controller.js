const responseFormatter = require("../helpers/responseFormatter.helper");
const {
  getDetailPengajuanSidang,
  updatePengajuanSeminarSidang,
} = require("../services/pengajuanSeminarSidang.service");
const {
  createSeminarSidang,
  updateSeminarSidang,
  getDetailSidang,
  getAllSidang,
  getSidang,
  getSidangPerbaikan,
  getSidangPenilaian,
  getSeminarSidangSuratTugas,
} = require("../services/seminarSidang.service");
const { v4: uuidv4 } = require("uuid");
const { createDok } = require("../services/dokumen.service");
const { updateStatus } = require("../services/aksesPengguna.service");
const { updatePengajuan } = require("../services/suratTugasDospem.service");
const dokumen = {
  jadwalSidang: 23,
  perbaikanSkripsi: 24,
  beritaAcaraSidang: 25,
  penilaianSidang: 26,
  penilaianDospem1: 47,
  penilaianDospem2: 48,
  penilaianKPenguji: 49,
  penilaianPenguji1: 50,
  penilaianPenguji2: 51,
};
const pathDocument = process.env.BASE_URL + "documents/";

class SidangControlller {
  static async createJadwal(req, res, next) {
    try {
      const pengajuanSidang = await getDetailPengajuanSidang({
        id: req.body.pengajuanSeminarSidangId,
      });
      if (!pengajuanSidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data pengajuan sidang tidak ditemukan",
        };
      }

      const sidang = await createSeminarSidang({
        uuid: uuidv4(),
        waktu: new Date(req.body.waktu),
        tempat: req.body.tempat,
        k_penguji: req.body.k_penguji,
        penguji1: req.body.penguji1,
        penguji2: req.body.penguji2,
      });

      await updatePengajuanSeminarSidang(
        { seminarSidangId: sidang.id },
        { id: pengajuanSidang.id }
      );

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.jadwalSidang,
          seminarSidangId: sidang.id,
          suratTugasDospemId: pengajuanSidang.SuratTugasDospem.id,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan jadwal ujian skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async updateJadwal(req, res, next) {
    try {
      const sidang = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: null,
        deletedAt: null,
      });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data jadwal ujian skripsi tidak ditemukan",
        };
      }

      let waktu = sidang.waktu;
      if (req.body.waktu) {
        waktu = new Date(req.body.waktu);
      }

      const updatedSidang = await updateSeminarSidang(
        {
          waktu,
          tempat: req.body.tempat || sidang.tempat,
          k_penguji: req.body.k_penguji || sidang.k_penguji,
          penguji1: req.body.penguji1 || sidang.penguji1,
          penguji2: req.body.penguji2 || sidang.penguji2,
        },
        {
          id: sidang.id,
        }
      );

      if (req.file) {
        await createDok({
          uuid: uuidv4(),
          jenisDokumenId: dokumen.jadwalSidang,
          seminarSidangId: sidang.id,
          suratTugasDospemId: sidang.PengajuanSeminarSidang.SuratTugasDospem.id,
          filename: req.file.filename,
          filesize: req.file.size,
          mimetype: req.file.mimetype,
          path: pathDocument + req.file.filename,
        });
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil memperbarui jadwal ujian skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteJadwal(req, res, next) {
    try {
      const sidang = await getSidang({
        uuid: req.params.uuid,
        deletedAt: null,
      });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data jadwal ujian skripsi tidak ditemukan",
        };
      }

      await updateSeminarSidang({ deletedAt: new Date() }, { id: sidang.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil menghapus jadwal ujian skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async listJadwal(req, res, next) {
    try {
      const listJadwal = await getAllSidang({
        deletedAt: null,
        keterlaksanaan: null,
      });
      if (listJadwal.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar jadwal ujian skripsi sedang kosong",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Daftar jadwal ujian skripsi ditemukan",
        listJadwal
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailJadwal(req, res, next) {
    try {
      const detailJadwal = await getDetailSidang({
        uuid: req.params.uuid,
        keterlaksanaan: null,
        deletedAt: null,
      });
      if (!detailJadwal) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail jadwal tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Detail jadwal ujian skripsi ditemukan",
        detailJadwal
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadBeritaAcara(req, res, next) {
    try {
      const sidang = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: null,
        deletedAt: null,
      });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Detail jadwal sidang tidak ditemukan",
        };
      }

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.beritaAcaraSidang,
        seminarSidangId: sidang.id,
        suratTugasDospemId: sidang.PengajuanSeminarSidang.SuratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      await updateSeminarSidang(
        {
          keterlaksanaan: true,
        },
        {
          id: sidang.id,
        }
      );

      await updateStatus(
        6,
        sidang.PengajuanSeminarSidang.SuratTugasDospem.mahasiswaId
      );

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah berita acara ujian skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadPerbaikan(req, res, next) {
    try {
      const sidang = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: true,
        deletedAt: null,
      });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data sidang tidak ditemukan",
        };
      }

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.perbaikanSkripsi,
        seminarSidangId: sidang.id,
        suratTugasDospemId: sidang.PengajuanSeminarSidang.SuratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      await updateStatus(
        7,
        sidang.PengajuanSeminarSidang.SuratTugasDospem.mahasiswaId
      );

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah dokumen perbaikan skripsi"
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailPerbaikan(req, res, next) {
    try {
      const sidang = await getSidangPerbaikan({ uuid: req.params.uuid });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data perbaikan skripsi tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data perbaikan skripsi ditemukan",
        sidang
      );
    } catch (error) {
      next(error);
    }
  }

  static async uploadPenilaian(req, res, next) {
    try {
      let sidang = await getSeminarSidangSuratTugas({
        uuid: req.params.uuid,
        keterlaksanaan: true,
        deletedAt: null,
      });

      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data sidang tidak ditemukan",
        };
      }

      let jenisDokumenId = null;
      let formNilai = {};

      switch (true) {
        case req.user.id == sidang.k_penguji:
          jenisDokumenId = dokumen.penilaianKPenguji;
          formNilai.nilaiKPenguji = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id == sidang.penguji1:
          jenisDokumenId = dokumen.penilaianPenguji1;
          formNilai.nilaiPenguji1 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id == sidang.penguji2:
          jenisDokumenId = dokumen.penilaianPenguji2;
          formNilai.nilaiPenguji2 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id ==
          sidang.PengajuanSeminarSidang.SuratTugasDospem.pembimbing1:
          jenisDokumenId = dokumen.penilaianDospem1;
          formNilai.nilaiDospem1 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        case req.user.id ==
          sidang.PengajuanSeminarSidang.SuratTugasDospem.pembimbing2:
          jenisDokumenId = dokumen.penilaianDospem2;
          formNilai.nilaiDospem2 = parseFloat(req.body.nilai).toPrecision(3);
          break;

        default:
          break;
      }

      if (!jenisDokumenId) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Anda tidak berhak mengupload penilian ujian skripsi ini",
        };
      }

      await updateSeminarSidang(formNilai, { id: sidang.id });

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId,
        seminarSidangId: sidang.id,
        suratTugasDospemId: sidang.PengajuanSeminarSidang.SuratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah dokumen hasil penilaian ujian skripsi"
      );

      sidang = await getSeminarSidangSuratTugas({
        id: sidang.id,
        keterlaksanaan: true,
        deletedAt: null,
      });

      let nilaiAkhir =
        (sidang.nilaiDospem1 +
          sidang.nilaiDospem2 +
          sidang.nilaiKPenguji +
          sidang.nilaiPenguji1 +
          sidang.nilaiPenguji2) /
        5;
      console.log(nilaiAkhir);

      await updatePengajuan(
        {
          nilaiSidang: parseFloat(nilaiAkhir).toPrecision(3),
        },
        { id: sidang.PengajuanSeminarSidang.SuratTugasDospem.id }
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailPenilaian(req, res, next) {
    try {
      const sidang = await getSidangPenilaian({ uuid: req.params.uuid });
      if (!sidang) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data perbaikan skripsi tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data perbaikan skripsi ditemukan",
        sidang
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = SidangControlller;
