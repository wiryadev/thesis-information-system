const ResponseFormatter = require("../helpers/responseFormatter.helper");
const {
  getDokumenByJenis,
  getDokumenSuratTugas,
  getDokumenSuratTugasDospemSeminarSidang,
} = require("../services/dokumen.service");

class ArsipController {
  static async arsipSkripsi(req, res, next) {
    try {
      const daftarSkripsi = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 24,
        deletedAt: null,
      });
      if (daftarSkripsi.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip skripsi kosong",
        };
      }

      ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip skripsi ditemukan",
        daftarSkripsi
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipPenilaian(req, res, next) {
    try {
      let arsipPenilaian;

      const penilaianSempro = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 11,
        deletedAt: null,
      });
      const penilaianSidang = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 26,
        deletedAt: null,
      });

      arsipPenilaian = penilaianSempro.concat(penilaianSidang);

      if (req.query) {
        if (req.query.filter == "seminar") {
          arsipPenilaian = penilaianSempro;
        } else {
          if (req.query.filter == "sidang") {
            arsipPenilaian = penilaianSidang;
          }
        }
      }

      if (arsipPenilaian.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip skripsi kosong",
        };
      }

      ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip penilaian ditemukan",
        arsipPenilaian
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipSuratTugas(req, res, next) {
    try {
      const daftarSuratTugas = await getDokumenSuratTugas({
        jenisDokumenId: 2,
        deletedAt: null,
      });
      if (daftarSuratTugas.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip surat tugas kosong",
        };
      }

      ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip surat tugas ditemukan",
        daftarSuratTugas
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipPerpanjanganSuratTugas(req, res, next) {
    try {
      const daftarPerpanjanganSuratTugas = await getDokumenSuratTugas({
        jenisDokumenId: 4,
        deletedAt: null,
      });

      if (daftarPerpanjanganSuratTugas.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip perpanjangan surat tugas dosen pembimbing kosong",
        };
      }

      ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip perpanjangan surat tugas dosen pembimbing ditemukan",
        daftarPerpanjanganSuratTugas
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipBeritaAcara(req, res, next) {
    try {
      let arsipBeritaAcara;

      const beritaAcaraSempro = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 10,
        deletedAt: null,
      });
      const beritaAcaraSidang = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 25,
        deletedAt: null,
      });

      arsipBeritaAcara = beritaAcaraSempro.concat(beritaAcaraSidang);

      if (req.query) {
        if (req.query.filter == "seminar") {
          arsipBeritaAcara = beritaAcaraSempro;
        } else {
          if (req.query.filter == "sidang") {
            arsipBeritaAcara = beritaAcaraSidang;
          }
        }
      }

      if (arsipBeritaAcara.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip berita acara kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip berita acara ditemukan",
        arsipBeritaAcara
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipJadwal(req, res, next) {
    try {
      const jadwalSempro = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 8,
        deletedAt: null,
      });
      const jadwalSidang = await getDokumenSuratTugasDospemSeminarSidang({
        jenisDokumenId: 23,
        deletedAt: null,
      });

      let arsipJadwal = jadwalSempro.concat(jadwalSidang);

      if (req.query) {
        if (req.query.filter == "seminar") {
          arsipJadwal = jadwalSempro;
        } else {
          if (req.query.filter == "sidang") {
            arsipJadwal = jadwalSidang;
          }
        }
      }

      if (arsipJadwal.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip jadwal seminar proposal dan ujian skripsi kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip jadwal seminar proposal dan ujian skripsi ditemukan",
        arsipJadwal
      );
    } catch (error) {
      next(error);
    }
  }

  static async arsipSuratIzinPenelitian(req, res, next) {
    try {
      const arsipSuratIzinPenelitian = await getDokumenSuratTugas({
        jenisDokumenId: 270,
        deletedAt: null,
      });

      if (arsipSuratIzinPenelitian.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Arsip surat izin penelitian kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Arsip surat izin penelitian ditemukan",
        arsipSuratIzinPenelitian
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ArsipController;
