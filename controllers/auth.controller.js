const {
  getUserByEmail,
  getUserActiveByEmail,
  updateUser,
} = require("../services/pengguna.service");
const bcrypt = require("bcryptjs");
const { generateJwt } = require("../services/jwt.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const otpGenerator = require("otp-generator");
const sendMail = require("../helpers/sendEmail.helper");
class AuthController {
  static async login(req, res, next) {
    try {
      const user = await getUserActiveByEmail(req.body.email);
      if (!user) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Email atau password salah",
        };
      }

      if (!bcrypt.compareSync(req.body.password, user.password)) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Email atau password salah",
        };
      }

      let akses = [];
      user.AksesPengguna.forEach((element) => {
        akses.push(element.aksesId);
      });

      const access_token = await generateJwt({
        id: user.id,
        uuid: user.uuid,
        email: user.email,
      });

      await updateUser({ access_token }, { id: user.id });

      return responseFormatter.success(res, 200, "OK", "Berhasil Login", {
        access_token,
        akses,
      });
    } catch (error) {
      next(error);
    }
  }

  static async logout(req, res, next) {
    try {
      const user = await getUserByEmail(req.body.email);
      if (!user) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Email tidak terdaftar",
        };
      }

      await updateUser({ access_token: null }, { id: user.id });

      return responseFormatter.success(res, 200, "OK", "Berhasil Logout");
    } catch (error) {
      next(error);
    }
  }

  static async forgotPassword(req, res, next) {
    try {
      const user = await getUserActiveByEmail(req.body.email);

      if (!user) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Email tidak terdaftar",
        };
      }

      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
      });

      const otpHashed = bcrypt.hashSync(otp, 10);
      const newDate = new Date();
      const otpExpiredAt = new Date(
        newDate.setMinutes(newDate.getMinutes() + 5)
      );

      await updateUser(
        {
          forgot_pass_token: otpHashed,
          forgot_pass_token_expiredAt: otpExpiredAt,
        },
        { id: user.id }
      );

      responseFormatter.success(
        res,
        200,
        "OK",
        "Informasi perubahan password dikirimkan melalui email anda, silahkan periksa kotak masuk email anda"
      );

      await sendMail(
        "s1ptik@unj.ac.id",
        user.email,
        "Forgot Password Sistem Informasi Skripsi",
        null,
        `<p>token: ${otp}</p><p>expiredAt: ${otpExpiredAt}</p>`
      );
    } catch (error) {
      next(error);
    }
  }

  static async verifyToken(req, res, next) {
    try {
      const user = await getUserActiveByEmail(req.body.email);

      if (!user) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Email atau token tidak valid",
        };
      }

      if (!bcrypt.compareSync(req.body.token, user.forgot_pass_token)) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Email atau token tidak valid",
        };
      }

      if (new Date() > user.forgot_pass_token_expiredAt) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Token kadaluwarsa",
        };
      }

      return responseFormatter.success(res, 200, "OK", "Token valid", {
        valid: true,
      });
    } catch (error) {
      next(error);
    }
  }

  static async changePassword(req, res, next) {
    try {
      const user = await getUserActiveByEmail(req.body.email);

      if (!user) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Email tidak valid",
        };
      }

      if (req.body.password !== req.body.password_confirmation) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Konfirmasi password tidak sesuai",
        };
      }

      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(req.body.password, salt);

      await updateUser(
        {
          password: hashedPassword,
          forgot_pass_token: null,
          forgot_pass_token_expiredAt: null,
        },
        { id: user.id }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil mengubah password"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthController;
