const ResponseFormatter = require("../helpers/responseFormatter.helper");
const { daftarStatusMahasiswa } = require("../services/aksesPengguna.service");
const { getAllDosenBimbingan } = require("../services/pengguna.service");
const {
  getBimbingan,
  getSkripsiBimbingan,
} = require("../services/suratTugasDospem.service");

class DaftarController {
  static async mahasiswaBimbingan(req, res, next) {
    try {
      const pembimbing1 = await getBimbingan({
        pembimbing1: req.user.id,
        persetujuan: true,
      });

      const pembimbing2 = await getBimbingan({
        pembimbing2: req.user.id,
        persetujuan: true,
      });

      const daftarMhs = pembimbing1.concat(pembimbing2);

      if (daftarMhs.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          messsage: "Anda tidak memiliki mahasiswa bimbingan",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Data mahasiswa bimbingan ditemukan",
        daftarMhs
      );
    } catch (error) {
      next(error);
    }
  }

  static async dosenBimbingan(req, res, next) {
    try {
      const daftarDosen = await getAllDosenBimbingan();
      if (daftarDosen.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Daftar dosen tidak ditemukan",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Data dosen dan mahasiswa bimbingan ditemukan",
        daftarDosen
      );
    } catch (error) {
      next(error);
    }
  }

  static async skripsiBimbingan(req, res, next) {
    try {
      const pembimbing1 = await getSkripsiBimbingan({
        pembimbing1: req.user.id,
        persetujuan: true,
      });

      const pembimbing2 = await getSkripsiBimbingan({
        pembimbing2: req.user.id,
        persetujuan: true,
      });

      const daftarMhs = pembimbing1.concat(pembimbing2);

      if (daftarMhs.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          messsage: "Anda tidak memiliki skripsi mahasiswa bimbingan",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Data skrispi mahasiswa bimbingan ditemukan",
        daftarMhs
      );
    } catch (error) {
      next(error);
    }
  }

  static async mahasiswaSkripsi(req, res, next) {
    try {
      const mahasiswaSkripsi = await daftarStatusMahasiswa();

      if (mahasiswaSkripsi.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Daftar mahasiswa yang sedang dalam penyusunan skripsi kosong",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Daftar mahasiswa yang sedang dalam penyusunan skripsi ditemukan",
        mahasiswaSkripsi
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = DaftarController;
