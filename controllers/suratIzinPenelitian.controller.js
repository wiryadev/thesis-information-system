const ResponseFormatter = require("../helpers/responseFormatter.helper");
const {
  getDetailSuratTugas,
  getDokumenSuratIzinPenelitian,
} = require("../services/suratTugasDospem.service");
const pathDocument = process.env.BASE_URL + "documents/";
const dokumen = { suratIzinPenelitian: 27 };
const { v4: uuidv4 } = require("uuid");
const { createDok } = require("../services/dokumen.service");

class SuratIzinPenelitianController {
  static async uploadSuratIzinPenelitian(req, res, next) {
    try {
      const suratTugasDospem = await getDetailSuratTugas({
        mahasiswaId: req.user.id,
      });
      if (!suratTugasDospem) {
        throw {
          code: 404,
          status: "Not Found",
          message:
            "Surat tugas dospem tidak ditemukan, anda harus mengajukan penetapan dospem terlebih dahulu",
        };
      }

      await createDok({
        uuid: uuidv4(),
        jenisDokumenId: dokumen.suratIzinPenelitian,
        suratTugasDospemId: suratTugasDospem.id,
        filename: req.file.filename,
        filesize: req.file.size,
        mimetype: req.file.mimetype,
        path: pathDocument + req.file.filename,
      });

      return ResponseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil mengunggah surat izin penelitian"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getSuratIzinPenelitian(req, res, next) {
    try {
      const DokumenSuratIzinPenelitian = await getDokumenSuratIzinPenelitian({
        mahasiswaId: req.user.id,
      });
      if (!DokumenSuratIzinPenelitian) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data surat izin penelitian tidak ditemukan",
        };
      }

      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Surat izin penelitian ditemukan",
        DokumenSuratIzinPenelitian.Dokumen[0]
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = SuratIzinPenelitianController;
