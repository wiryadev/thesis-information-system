const {
  Dokumen,
  JenisDokumen,
  SuratTugasDospem,
  PengajuanSeminarSidang,
  SeminarSidang,
  Pengguna,
  BiodataPengguna,
} = require("../models");

const createDok = async (form) => {
  return await Dokumen.create(form);
};

const getDokumenByJenis = async (jenis) => {
  return await Dokumen.findAll({
    where: { jenisDokumenId: jenis, deletedAt: null },
    attributes: ["id", "jenisDokumenId", "path"],
    include: {
      model: SeminarSidang,
      as: "SeminarSidang",
      attributes: ["id"],
      include: {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        attributes: ["id"],
        include: {
          model: SuratTugasDospem,
          as: "SuratTugasDospem",
        },
      },
    },
  });
};

const getDokumenSuratTugasDospemSeminarSidang = async (where) => {
  return await Dokumen.findAll({
    where,
    attributes: ["id", "path"],
    include: [
      {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["nama"],
      },
      {
        model: SeminarSidang,
        as: "SeminarSidang",
        include: [
          {
            model: PengajuanSeminarSidang,
            as: "PengajuanSeminarSidang",
            attributes: ["seminar", "sidang"],
          },
          {
            model: Pengguna,
            as: "KetuaPenguji",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nidn", "nip"],
            },
          },
          {
            model: Pengguna,
            as: "Penguji1",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nidn", "nip"],
            },
          },
          {
            model: Pengguna,
            as: "Penguji2",
            attributes: ["id", "uuid", "email"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["nama", "nidn", "nip"],
            },
          },
        ],
      },
      {
        model: SuratTugasDospem,
        as: "SuratTugasDospem",
        include: [
          {
            model: Pengguna,
            as: "Mahasiswa",
            attributes: ["id"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["id", "nama", "nim", "angkatan"],
            },
          },
          {
            model: Pengguna,
            as: "Pembimbing1",
            attributes: ["id"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["id", "nama", "nip", "nidn"],
            },
          },
          {
            model: Pengguna,
            as: "Pembimbing2",
            attributes: ["id"],
            include: {
              model: BiodataPengguna,
              as: "Biodata",
              attributes: ["id", "nama", "nip", "nidn"],
            },
          },
        ],
      },
    ],
  });
};

const getDokumenSuratTugas = async (where) => {
  return await Dokumen.findAll({
    where,
    attributes: ["id", "path"],
    include: {
      model: SuratTugasDospem,
      as: "SuratTugasDospem",
      include: [
        {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["id", "nama", "nim", "angkatan"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing1",
          attributes: ["id"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["id", "nama", "nip", "nidn"],
          },
        },
        {
          model: Pengguna,
          as: "Pembimbing2",
          attributes: ["id"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["id", "nama", "nip", "nidn"],
          },
        },
      ],
    },
  });
};

module.exports = {
  createDok,
  getDokumenByJenis,
  getDokumenSuratTugas,
  getDokumenSuratTugasDospemSeminarSidang,
};
