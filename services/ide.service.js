const { IdePenelitian, Pengguna, BiodataPengguna } = require("../models");
const { createDok } = require("./dokumen.service");

const insertIdePenelitian = async (form) => {
  await IdePenelitian.create(form);
};

const selectAllIdeas = async () => {
  return await IdePenelitian.findAll({
    where: { pelaksana: null, deletedAt: null },
    attributes: ["id", "uuid", "judul"],
    include: {
      model: Pengguna,
      as: "Dosen",
      attributes: ["id", "uuid", "email"],
      include: {
        model: BiodataPengguna,
        as: "Biodata",
        attributes: ["nama"],
      },
    },
  });
};

const selectOneIdea = async (where) => {
  return await IdePenelitian.findOne({
    where,
    include: {
      model: Pengguna,
      as: "Dosen",
      attributes: ["id", "uuid", "email"],
      include: {
        model: BiodataPengguna,
        as: "Biodata",
        attributes: ["nama"],
      },
    },
  });
};

const softDeleteIdea = async (where) => {
  await IdePenelitian.update({ deletedAt: new Date() }, { where });
};

module.exports = {
  insertIdePenelitian,
  selectAllIdeas,
  selectOneIdea,
  softDeleteIdea,
};
