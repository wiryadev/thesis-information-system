const {
  SuratTugasDospem,
  Pengguna,
  Dokumen,
  BiodataPengguna,
  JenisDokumen,
  PengajuanSeminarSidang,
  SeminarSidang,
} = require("../models");
const { Op } = require("sequelize");
const { createDok } = require("../services/dokumen.service");

const getDetailSuratTugas = async (where) => {
  const suratTugas = await SuratTugasDospem.findOne({ where });
  return suratTugas;
};

const createPengajuan = async (formPengajuan, formDokumen) => {
  const pengajuan = await SuratTugasDospem.create(formPengajuan);
  formDokumen.suratTugasDospemId = pengajuan.id;
  await createDok(formDokumen);
  return pengajuan;
};

const getAllPengajuanDospem = async () => {
  const pengajuan = await SuratTugasDospem.findAll({
    where: { persetujuan: null },
    attributes: ["id", "uuid", "judul", "deskripsi", "keterangan"],
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Dokumen,
        as: "Dokumen",
        attributes: ["id", "uuid", "filename", "filesize", "path"],
        include: {
          model: JenisDokumen,
          as: "JenisDokumen",
          attributes: ["nama"],
        },
      },
    ],
  });
  return pengajuan;
};

const getSuratTugasBy = async (where) => {
  const suratTugas = await SuratTugasDospem.findOne({ where });
  return suratTugas;
};

const getBimbingan = async (where) => {
  return await SuratTugasDospem.findAll({
    where,
    attributes: ["id", "uuid", "judul"],
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
    ],
  });
};
const getSkripsiBimbingan = async (where) => {
  return await SuratTugasDospem.findAll({
    where,
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        attributes: ["sidang"],
        where: {
          sidang: true,
        },
        include: {
          model: SeminarSidang,
          as: "SeminarSidang",
          attributes: ["uuid"],
          where: { keterlaksanaan: true },
          include: {
            model: Dokumen,
            as: "DokumenSeminarSidang",
            where: { jenisDokumenId: 24 },
            attributes: ["id", "uuid", "filename", "filesize", "path"],
            include: {
              model: JenisDokumen,
              as: "JenisDokumen",
              attributes: ["id", "nama"],
            },
          },
        },
      },
    ],
  });
};

const getDetailPengajuanDospemByUuid = async (uuid) => {
  const pengajuan = await SuratTugasDospem.findOne({
    where: { uuid },
    attributes: [
      "id",
      "uuid",
      "judul",
      "deskripsi",
      "keterangan",
      "persetujuan",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Dokumen,
        as: "Dokumen",
        attributes: ["id", "uuid", "path"],
        include: {
          model: JenisDokumen,
          as: "JenisDokumen",
          attributes: ["nama"],
        },
      },
    ],
  });
  return pengajuan;
};

const updatePengajuan = async (form, where) => {
  return await SuratTugasDospem.update(form, { where, returning: true });
};

const getAllPengajuaPerpanjanganDospem = async () => {
  const perpanjangan = await SuratTugasDospem.findAll({
    where: { persetujuan: null },
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
    ],
  });
  return perpanjangan;
};

const getDetailPerpanjanganByUuid = async (uuid) => {
  const dataPerpanjangan = await SuratTugasDospem.findOne({
    where: { uuid },
    attributes: [
      "id",
      "uuid",
      "judul",
      "deskripsi",
      "keterangan",
      "persetujuan",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Pengguna,
        as: "Mahasiswa",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nim", "angkatan"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Pembimbing2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Dokumen,
        as: "Dokumen",
        attributes: ["id", "uuid", "path"],
        include: {
          model: JenisDokumen,
          as: "JenisDokumen",
          attributes: ["nama"],
          where: {
            id: 3,
          },
        },
      },
    ],
  });
  return dataPerpanjangan;
};

const getDokumenSuratIzinPenelitian = async () => {
  return await SuratTugasDospem.findOne({
    attributes: [],
    include: {
      model: Dokumen,
      as: "Dokumen",
      attributes: ["id", "uuid", "filename", "filesize", "path"],
      include: {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["nama"],
      },
      where: { jenisDokumenId: 27 },
    },
    order: [[Dokumen, "id", "DESC"]],
  });
};

module.exports = {
  createPengajuan,
  getAllPengajuanDospem,
  getDetailPengajuanDospemByUuid,
  updatePengajuan,
  getSuratTugasBy,
  getAllPengajuaPerpanjanganDospem,
  getDetailPerpanjanganByUuid,
  getDetailSuratTugas,
  getBimbingan,
  getSkripsiBimbingan,
  getDokumenSuratIzinPenelitian,
};
