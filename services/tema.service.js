const { Minat } = require("../models");

const insertTema = async (form) => {
  await Minat.create(form);
};

const selectOneTema = async (where) => {
  return await Minat.findOne({ where });
};

const softDeleteTema = async (form, where) => {
  await Minat.update(form, { where });
};

module.exports = { insertTema, selectOneTema, softDeleteTema };
