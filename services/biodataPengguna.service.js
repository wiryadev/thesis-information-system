const { Pengguna, BiodataPengguna } = require("../models");

const createBio = async (formBio) => {
  console.log(formBio);
  const biodata = await BiodataPengguna.create(formBio);
  return biodata;
};

const updateBio = async (form, where) => {
  return await BiodataPengguna.update(form, { where });
};

module.exports = { createBio, updateBio };
