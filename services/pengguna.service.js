const {
  Pengguna,
  AksesPengguna,
  BiodataPengguna,
  SuratTugasDospem,
  Minat,
} = require("../models");
const { createBio } = require("./biodataPengguna.service");
const { createAksesPengguna } = require("./aksesPengguna.service");

const getAllUsers = async () => {
  return await Pengguna.findAll({
    where: {
      active: true,
      deletedAt: null,
    },
    attributes: ["id", "uuid", "email"],
    order: [["id", "DESC"]],
    include: {
      model: BiodataPengguna,
      as: "Biodata",
      attributes: ["id", "nama", "nim", "nip", "nidn", "angkatan"],
    },
  });
};

const getAllInfoUsers = async () => {
  return await Pengguna.findAll({
    where: {
      active: true,
      deletedAt: null,
    },
    include: [
      {
        model: BiodataPengguna,
        as: "Biodata",
      },
      {
        model: AksesPengguna,
        as: "AksesPengguna",
      },
    ],
  });
};

const getUserByEmail = async (email) => {
  return await Pengguna.findOne({
    where: { email },
  });
};

const getUserBioTema = async (where) => {
  return await Pengguna.findOne({
    where,
    attributes: ["id", "uuid", "email"],

    include: {
      model: BiodataPengguna,
      as: "Biodata",
      attributes: ["id", "nama"],
      include: {
        model: Minat,
        as: "Minat",
        where: { deletedAt: null },
        attributes: ["nama"],
      },
    },
  });
};

const getUserBioBy = async (where) => {
  return await Pengguna.findOne({
    where,
    attributes: ["id", "uuid", "email", "createdAt", "updatedAt"],

    include: {
      model: BiodataPengguna,
      as: "Biodata",
      attributes: [
        "id",
        "nama",
        "nim",
        "nip",
        "nidn",
        "angkatan",
        "no_hp",
        "createdAt",
        "updatedAt",
      ],
      include: {
        model: Minat,
        as: "Minat",
      },
    },
  });
};

const getUserActiveByEmail = async (email) => {
  return await Pengguna.findOne({
    where: { email, active: true, deletedAt: null },
    include: {
      model: AksesPengguna,
      as: "AksesPengguna",
      attributes: ["aksesId"],
    },
  });
};

const getUserByUuid = async (uuid) => {
  return await Pengguna.findOne({
    where: { uuid },
  });
};

const getUserActiveByUuid = async (uuid) => {
  return await Pengguna.findOne({
    where: { uuid, active: true, deletedAt: null },
  });
};

const getUserAccessByUuid = async (uuid) => {
  return await Pengguna.findOne({
    where: { uuid },
    attributes: ["uuid", "email", "active", "access_token"],
    include: {
      model: AksesPengguna,
      as: "AksesPengguna",
      attributes: ["aksesId"],
    },
  });
};

const getActiveUserAccessByUuid = async (uuid) => {
  return await Pengguna.findOne({
    where: { uuid },
    attributes: ["uuid", "email", "active", "access_token"],
    include: {
      model: AksesPengguna,
      as: "AksesPengguna",
      attributes: ["aksesId"],
    },
  });
};

const createUser = async (formUser, formBio, formAkses) => {
  const biodata = await createBio(formBio);

  formUser.biodataId = biodata.id;
  const newUser = await Pengguna.create(formUser);

  formAkses.penggunaId = newUser.id;

  const aksesPengguna = await createAksesPengguna(formAkses);
  return newUser;
};

const updateUser = async (form, where) => {
  return await Pengguna.update(form, {
    where,
  }).catch((err) => console.log(err));
};

const softDeleteUser = async (where) => {
  return await Pengguna.update(
    { active: false, deletedAt: new Date() },
    {
      where,
    }
  ).catch((err) => console.log(err));
};

const getAllDosenBimbingan = async () => {
  return await Pengguna.findAll({
    attributes: ["id", "uuid", "email"],
    include: [
      {
        model: AksesPengguna,
        as: "AksesPengguna",
        where: { aksesId: 2 },
        attributes: ["aksesId"],
      },
      {
        model: BiodataPengguna,
        as: "Biodata",
        attributes: ["id", "nama"],
      },
      {
        model: SuratTugasDospem,
        as: "SuratTugasPembimbing1",
        // where: { persetujuan: true },
        attributes: ["id", "uuid", "judul"],
        include: {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nim", "angkatan"],
          },
        },
      },
      {
        model: SuratTugasDospem,
        as: "SuratTugasPembimbing2",
        // where: { persetujuan: true },
        attributes: ["id", "uuid", "judul"],
        include: {
          model: Pengguna,
          as: "Mahasiswa",
          attributes: ["id", "uuid", "email"],
          include: {
            model: BiodataPengguna,
            as: "Biodata",
            attributes: ["nama", "nim", "angkatan"],
          },
        },
      },
    ],
  });
};

module.exports = {
  getUserByEmail,
  getUserByUuid,
  getUserActiveByUuid,
  getActiveUserAccessByUuid,
  getUserAccessByUuid,
  getUserActiveByEmail,
  updateUser,
  createUser,
  getAllUsers,
  getAllInfoUsers,
  softDeleteUser,
  getUserBioBy,
  getAllDosenBimbingan,
  getUserBioTema,
};
