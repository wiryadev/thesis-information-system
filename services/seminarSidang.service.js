const { Op } = require("sequelize");
const {
  SeminarSidang,
  PengajuanSeminarSidang,
  SuratTugasDospem,
  Pengguna,
  Dokumen,
  BiodataPengguna,
  JenisDokumen,
} = require("../models");

const createSeminarSidang = async (form) => {
  return await SeminarSidang.create(form);
};

const getSeminarSidang = async (where) => {
  return await SeminarSidang.findOne({ where });
};

const getSeminarSidangSuratTugas = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: {
      model: PengajuanSeminarSidang,
      as: "PengajuanSeminarSidang",
      attributes: ["id"],
      include: {
        model: SuratTugasDospem,
        as: "SuratTugasDospem",
        attributes: ["id", "mahasiswaId", "pembimbing1", "pembimbing2"],
      },
    },
  });
};

const getSidang = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: [
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        where: {
          sidang: true,
        },
      },
    ],
  });
};

const getSidangPerbaikan = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: {
      model: Dokumen,
      as: "DokumenSeminarSidang",
      attributes: ["id", "uuid", "path"],
      where: {
        jenisDokumenId: 24,
      },
      order: [["id", "DESC"]],
      limit: 1,
      include: {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["id", "nama"],
      },
    },
  });
};

const getSeminarPerbaikan = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: {
      model: Dokumen,
      as: "DokumenSeminarSidang",
      attributes: ["id", "uuid", "path"],
      where: {
        jenisDokumenId: 9,
      },
      order: [["id", "DESC"]],
      limit: 1,
      include: {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["nama"],
      },
    },
  });
};

const getSidangPenilaian = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: {
      model: Dokumen,
      as: "DokumenSeminarSidang",
      attributes: ["id", "uuid", "path"],
      where: {
        jenisDokumenId: {
          [Op.or]: [47, 48, 49, 50, 51],
        },
      },
      order: [["id", "DESC"]],
      limit: 1,
      include: {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["id", "nama"],
      },
    },
  });
};

const getSeminarPenilaian = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: {
      model: Dokumen,
      as: "DokumenSeminarSidang",
      attributes: ["id", "uuid", "path"],
      where: {
        jenisDokumenId: {
          [Op.or]: [47, 48, 50, 51],
        },
      },
      include: {
        model: JenisDokumen,
        as: "JenisDokumen",
        attributes: ["id", "nama"],
      },
    },
  });
};

const getDetailSidang = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: [
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        where: {
          sidang: true,
        },
        include: [
          {
            model: SuratTugasDospem,
            as: "SuratTugasDospem",
            include: [
              {
                model: Pengguna,
                as: "Mahasiswa",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nim", "angkatan"],
                },
              },
              {
                model: Pengguna,
                as: "Pembimbing1",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nidn", "nip"],
                },
              },
              {
                model: Pengguna,
                as: "Pembimbing2",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nidn", "nip"],
                },
              },
              {
                model: Dokumen,
                as: "Dokumen",
                attributes: ["id", "uuid", "path"],
                include: {
                  model: JenisDokumen,
                  as: "JenisDokumen",
                  attributes: ["nama"],
                  where: {
                    id: 3,
                  },
                },
              },
            ],
          },
          {
            model: Dokumen,
            as: "DokumenPengajuanSeminarSidang",
            attributes: ["id", "uuid", "path"],
            include: {
              model: JenisDokumen,
              as: "JenisDokumen",
              attributes: ["nama"],
            },
          },
        ],
      },
      {
        model: Pengguna,
        as: "KetuaPenguji",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Dokumen,
        as: "DokumenSeminarSidang",
        attributes: ["id", "uuid", "path"],
        include: {
          model: JenisDokumen,
          as: "JenisDokumen",
          attributes: ["nama"],
        },
      },
    ],
  });
};

const getDetailSeminar = async (where) => {
  return await SeminarSidang.findOne({
    where,
    include: [
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        where: {
          seminar: true,
        },
        include: [
          {
            model: SuratTugasDospem,
            as: "SuratTugasDospem",
            include: [
              {
                model: Pengguna,
                as: "Mahasiswa",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nim", "angkatan"],
                },
              },
              {
                model: Pengguna,
                as: "Pembimbing1",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nidn", "nip"],
                },
              },
              {
                model: Pengguna,
                as: "Pembimbing2",
                attributes: ["id", "uuid", "email"],
                include: {
                  model: BiodataPengguna,
                  as: "Biodata",
                  attributes: ["nama", "nidn", "nip"],
                },
              },
              {
                model: Dokumen,
                as: "Dokumen",
                attributes: ["id", "uuid", "path", "jenisDokumenId"],
                where: { jenisDokumenId: 8 },
              },
            ],
          },
        ],
      },
      {
        model: Pengguna,
        as: "KetuaPenguji",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Dokumen,
        as: "DokumenSeminarSidang",
      },
    ],
  });
};

const getAllSidang = async (where) => {
  return await SeminarSidang.findAll({
    where,
    include: [
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        where: {
          sidang: true,
        },
        include: {
          model: SuratTugasDospem,
          as: "SuratTugasDospem",
          include: [
            {
              model: Pengguna,
              as: "Mahasiswa",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nim", "angkatan"],
              },
            },
            {
              model: Pengguna,
              as: "Pembimbing1",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nidn", "nip"],
              },
            },
            {
              model: Pengguna,
              as: "Pembimbing2",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nidn", "nip"],
              },
            },
          ],
        },
      },
      {
        model: Pengguna,
        as: "KetuaPenguji",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
    ],
  });
};

const getAllSeminar = async (where) => {
  return await SeminarSidang.findAll({
    where,
    include: [
      {
        model: PengajuanSeminarSidang,
        as: "PengajuanSeminarSidang",
        where: {
          seminar: true,
        },
        include: {
          model: SuratTugasDospem,
          as: "SuratTugasDospem",
          include: [
            {
              model: Pengguna,
              as: "Mahasiswa",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nim", "angkatan"],
              },
            },
            {
              model: Pengguna,
              as: "Pembimbing1",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nidn", "nip"],
              },
            },
            {
              model: Pengguna,
              as: "Pembimbing2",
              attributes: ["id", "uuid", "email"],
              include: {
                model: BiodataPengguna,
                as: "Biodata",
                attributes: ["nama", "nidn", "nip"],
              },
            },
          ],
        },
      },
      {
        model: Pengguna,
        as: "KetuaPenguji",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji1",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
      {
        model: Pengguna,
        as: "Penguji2",
        attributes: ["id", "uuid", "email"],
        include: {
          model: BiodataPengguna,
          as: "Biodata",
          attributes: ["nama", "nidn", "nip"],
        },
      },
    ],
  });
};

const updateSeminarSidang = async (form, where) => {
  return await SeminarSidang.update(form, {
    where,
    returning: true,
    plain: true,
  });
};

module.exports = {
  createSeminarSidang,
  getSeminarSidang,
  updateSeminarSidang,
  getAllSidang,
  getAllSeminar,
  getDetailSeminar,
  getSidang,
  getSeminarPerbaikan,
  getSeminarPenilaian,
  getDetailSidang,
  getSidangPerbaikan,
  getSidangPenilaian,
  getSeminarSidangSuratTugas,
};
