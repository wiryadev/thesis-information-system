const {
  PengajuanPenelitianIdeDosen,
  Pengguna,
  BiodataPengguna,
} = require("../models");
const { createDok } = require("./dokumen.service");

const insertPengajuanPenelitianDosen = async (form, form_dokumen) => {
  const newIdea = await PengajuanPenelitianIdeDosen.create(form);
  form_dokumen.pengajuanPenelitianDosenId = newIdea.id;
  await createDok(form_dokumen);
};

const selectAllPengajuanPenelitian = async (where) => {
  return await PengajuanPenelitianIdeDosen.findAll({
    where,
    include: {
      model: Pengguna,
      as: "Mahasiswa",
      attributes: ["id", "uuid", "email"],
      include: {
        model: BiodataPengguna,
        as: "Biodata",
        attributes: ["id", "nama", "nim", "angkatan"],
      },
    },
  });
};

const selectOnePengajuanPenelitian = async (where) => {
  return await PengajuanPenelitianIdeDosen.findOne({ where });
};

const updatePengajuanPenelitian = async (form, where) => {
  await PengajuanPenelitianIdeDosen.update(form, { where });
};

module.exports = {
  insertPengajuanPenelitianDosen,
  selectAllPengajuanPenelitian,
  selectOnePengajuanPenelitian,
  updatePengajuanPenelitian,
};
