const multer = require("multer");
const fs = require("fs");
const path = "public/documents";
const slug = require("slug");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }
    cb(null, path);
  },
  filename: (req, file, cb) => {
    const filename = file.originalname.split(".");
    const uniqueSuffix =
      Date.now() +
      "-" +
      req.user.email +
      "-" +
      slug(filename[0], "_") +
      "." +
      filename[1];
    cb(null, file.fieldname + "-" + uniqueSuffix);
  },
});

module.exports = storage;
