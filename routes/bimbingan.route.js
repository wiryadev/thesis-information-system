const router = require("express").Router();
const BimbinganController = require("../controllers/bimbingan.controller");
const { authorization } = require("../middlewares/auth.middlewares");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (!(file.mimetype === "image/jpeg" || file.mimetype === "image/png")) {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus jpeg/png" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware, authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("dokumentasi"),
  [
    check("topik", "Atribut topik tidak boleh kosong").notEmpty(),
    check("waktu", "Atribut waktu tidak boleh kosong").notEmpty(),
    check("tempat", "Atribut tempat tidak boleh kosong").notEmpty(),
  ],
  ValidationMiddleware.result,
  BimbinganController.createBimbingan
);

router.delete(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware, authorization(req, res, next, [akses.mahasiswa]);
  },
  BimbinganController.deleteBimbingan
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware,
      authorization(req, res, next, [
        akses.mahasiswa,
        akses.dosen,
        akses.admin,
      ]);
  },
  BimbinganController.getAllBimbingan
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware,
      authorization(req, res, next, [
        akses.mahasiswa,
        akses.dosen,
        akses.admin,
      ]);
  },
  BimbinganController.getDetailBimbingan
);

module.exports = router;
