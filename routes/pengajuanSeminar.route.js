const PengajuanSeminarController = require("../controllers/pengajuanSeminar.controller");
const router = require("express").Router();
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.fields([
    { name: "proposal_skripsi", maxCount: 1 },
    { name: "surat_bimbingan", maxCount: 2 },
    { name: "surat_pernyataan_dospem", maxCount: 2 },
  ]),
  ValidationMiddleware.files,
  PengajuanSeminarController.pengajuanSeminar
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.admin,
    ]);
  },
  PengajuanSeminarController.daftarPengajuanSeminar
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.koorprodi,
      akses.admin,
    ]);
  },
  PengajuanSeminarController.detailPengajuanSeminar
);

module.exports = router;
