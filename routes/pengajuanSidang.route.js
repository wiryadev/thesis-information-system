const router = require("express").Router();
const PengajuanSidangController = require("../controllers/pengajuanSidang.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "application/pdf" ||
      file.mimetype ===
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(
        { code: 400, status: "Bad Request", message: "Format Dokumen Salah" },
        false
      );
    }
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.fields([
    { name: "naskah_skripsi", maxCount: 1 },
    { name: "biodata", maxCount: 1 },
    { name: "surat_permohonan_ujian_skripsi", maxCount: 1 },
    { name: "lembar_persetujuan_dospem", maxCount: 1 },
    { name: "pra_transkrip", maxCount: 1 },
    { name: "photo", maxCount: 1 },
    { name: "scan_ktm", maxCount: 1 },
    { name: "surat_tugas_dan_lembar_bimbingan", maxCount: 1 },
    { name: "bukti_pkl", maxCount: 1 },
    { name: "bukti_pkm", maxCount: 1 },
    { name: "surat_pernyataan_dospem", maxCount: 2 },
  ]),
  ValidationMiddleware.files,
  ValidationMiddleware.pengajuanSidang,
  PengajuanSidangController.pengajuanSidang
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  PengajuanSidangController.daftarPengajuanSidang
);

router.get(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
    ]);
  },
  PengajuanSidangController.detailPengajuanSidang
);

module.exports = router;
