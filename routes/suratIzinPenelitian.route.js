const SuratIzinPenelitianController = require("../controllers/suratIzinPenelitian.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };
const router = require("express").Router();
const multer = require("multer");
const storage = require("../helpers/multerStorage.helper");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const upload = multer({
  storage,
  limits: { fileSize: 5000000 },
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== "application/pdf") {
      cb(
        { code: 400, status: "Bad Request", message: "Format harus pdf" },
        false
      );
    }
    cb(null, true);
  },
});

router.post(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.mahasiswa]);
  },
  upload.single("surat-izin-penelitian"),
  ValidationMiddleware.file,
  SuratIzinPenelitianController.uploadSuratIzinPenelitian
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.mahasiswa,
      akses.dosen,
    ]);
  },
  SuratIzinPenelitianController.getSuratIzinPenelitian
);

module.exports = router;
