const router = require("express").Router();
const { check } = require("express-validator");
const TemaController = require("../controllers/tema.controller");
const UserController = require("../controllers/user.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const akses = { koorprodi: 1, dosen: 2, admin: 3, mahasiswa: 4 };

router.post(
  "/",
  [
    check("email", "Email tidak boleh kosong").notEmpty(),
    check("akses", "Akses tidak boleh kosong").notEmpty(),
  ],
  AuthMiddleware.authentication,
  (req, res, next) =>
    AuthMiddleware.authorization(req, res, next, [akses.admin]),
  ValidationMiddleware.result,
  UserController.add
);

router.get(
  "/",
  AuthMiddleware.authentication,
  (req, res, next) =>
    AuthMiddleware.authorization(req, res, next, [akses.admin]),
  UserController.getAll
);

router.delete(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) =>
    AuthMiddleware.authorization(req, res, next, [akses.admin]),
  UserController.delete
);

router.get(
  "/biodata",
  AuthMiddleware.authentication,
  (req, res, next) =>
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.mahasiswa,
      akses.dosen,
    ]),
  UserController.showBiodata
);

router.put(
  "/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) =>
    AuthMiddleware.authorization(req, res, next, [
      akses.admin,
      akses.koorprodi,
      akses.mahasiswa,
      akses.dosen,
    ]),
  UserController.updateUserBiodata
);

router.post(
  "/tema",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen]);
  },
  [check("nama", "Atribut nama harus berupa Alphabet").notEmpty()],
  ValidationMiddleware.result,
  TemaController.addTema
);

router.get(
  "/:uuid/tema",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [
      akses.dosen,
      akses.admin,
      akses.mahasiswa,
    ]);
  },
  TemaController.getTema
);

router.delete(
  "/tema/:uuid",
  AuthMiddleware.authentication,
  (req, res, next) => {
    AuthMiddleware.authorization(req, res, next, [akses.dosen]);
  },
  TemaController.deleteTema
);

module.exports = router;
