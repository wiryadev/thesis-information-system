"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("BiodataPenggunas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      nama: {
        type: Sequelize.STRING(100),
      },
      nip: {
        type: Sequelize.STRING(20),
      },
      nidn: {
        type: Sequelize.STRING(20),
      },
      nim: {
        type: Sequelize.STRING(12),
      },
      angkatan: {
        type: Sequelize.CHAR(4),
      },
      no_hp: {
        type: Sequelize.STRING(15),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("BiodataPenggunas");
  },
};
