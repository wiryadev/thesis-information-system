"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("SeminarSidangs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      waktu: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      tempat: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      k_penguji: {
        type: Sequelize.INTEGER(5),
      },
      penguji1: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      penguji2: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      keterlaksanaan: {
        type: Sequelize.BOOLEAN,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("SeminarSidangs");
  },
};
