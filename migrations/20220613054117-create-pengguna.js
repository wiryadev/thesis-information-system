"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Penggunas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      biodataId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
        references: { model: "BiodataPenggunas", key: "id" },
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING(50),
        unique: true,
      },
      password: {
        type: Sequelize.STRING,
      },
      active: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      access_token: {
        type: Sequelize.TEXT,
      },
      forgot_pass_token: {
        type: Sequelize.TEXT,
      },
      verifiedAt: {
        type: Sequelize.DATE,
      },
      forgot_pass_token_expiredAt: {
        type: Sequelize.DATE,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Penggunas");
  },
};
