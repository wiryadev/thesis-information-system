"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("IdePenelitians", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      dosenId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      judul: {
        allowNull: false,
        type: Sequelize.STRING(100),
      },
      deskripsi: {
        type: Sequelize.TEXT,
      },
      pelaksana: {
        type: Sequelize.BOOLEAN,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("IdePenelitians");
  },
};
