"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("SuratTugasDospems", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        type: Sequelize.UUID,
      },
      mahasiswaId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
        references: { model: "Penggunas", key: "id" },
      },
      pembimbing1: {
        type: Sequelize.INTEGER(5),
        references: { model: "Penggunas", key: "id" },
      },
      pembimbing2: {
        type: Sequelize.INTEGER(5),
        references: { model: "Penggunas", key: "id" },
      },
      judul: {
        type: Sequelize.STRING,
      },
      deskripsi: {
        type: Sequelize.TEXT,
      },
      keterangan: {
        type: Sequelize.STRING,
      },
      persetujuan: {
        type: Sequelize.BOOLEAN,
      },
      expiredAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("SuratTugasDospems");
  },
};
