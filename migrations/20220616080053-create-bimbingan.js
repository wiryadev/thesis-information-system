"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Bimbingans", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(5),
      },
      uuid: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      suratTugasDospemId: {
        allowNull: false,
        type: Sequelize.INTEGER(5),
      },
      topik: {
        allowNull: false,
        type: Sequelize.STRING(100),
      },
      catatan: {
        type: Sequelize.TEXT,
      },
      waktu: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      tempat: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      dokumentasi: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Bimbingans");
  },
};
